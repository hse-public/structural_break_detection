#!/usr/bin/env python

import re
import itertools
import numpy as np
from datetime import datetime, timedelta
from sklearn import metrics

def root_mean_square_error(y_true, y_pred):
    return np.sqrt(metrics.mean_squared_error(y_true, y_pred))

def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

def mean_absolute_error(y_true, y_pred):
    return metrics.mean_absolute_error(y_true, y_pred)

def timeseries_evaluation_metrics(y_true, y_pred):
    MSE = metrics.mean_squared_error(y_true, y_pred)
    MAE = metrics.mean_absolute_error(y_true, y_pred)
    RMSE = root_mean_square_error(y_true, y_pred)
    MAPE = mean_absolute_percentage_error(y_true, y_pred)
    R2_score = metrics.r2_score(y_true, y_pred)
    return MSE, MAE, RMSE, MAPE, R2_score   

# from https://github.com/inderjalli/wmape_grouping/blob/master/wmape_group_example.py
def wmape_metric(actual, forecast):
    # we take two series and calculate an output a wmape from it,
    # not to be used in a grouping function
    actual = actual.to_numpy()
    forecast = forecast.to_numpy()
    # make a series called mape
    se_mape = abs(actual-forecast)/actual
    # get a float of the sum of the actual
    ft_actual_sum = actual.sum()
    # get a series of the multiple of the actual & the mape
    se_actual_prod_mape = actual * se_mape
    # summate the prod of the actual and the mape
    ft_actual_prod_mape_sum = se_actual_prod_mape.sum()
    # float: wmape of forecast
    ft_wmape_forecast = ft_actual_prod_mape_sum / ft_actual_sum
    # return a float
    return ft_wmape_forecast


def timeseries_evaluation_metrics_func(y_true, y_pred):
    MSE, MAE, RMSE, MAPE, R2_score = timeseries_evaluation_metrics(y_true, y_pred) 
    wmape = wmape_metric(y_true, y_pred)

    print('Evaluation metric results:-')
    print(f'Mean Square Error is : {MSE}')
    print(f'Mean Absolute Error is : {MAE}')
    print(f'Root Mean Square Error is : {RMSE}')
    print(f'Mean Absolute Percentage Error is : {MAPE}')
    print(f'R2 score is : {R2_score}',end='\n\n') 
    print(f"Weighted MAPE: {wmape}")


def get_nth_train_split(df_series, order_n, train_week_len=28, prediction_week_len=7):
    """
    Implement train-test split given a split number and return training, val and test data
    """

    min_date = df_series.index.min() + timedelta(weeks= order_n)
    test_lower_date = min_date + timedelta(weeks=train_week_len) + timedelta(days=prediction_week_len) + timedelta(minutes=1)
    test_upper_date = min_date + timedelta(weeks=train_week_len) + timedelta(days=prediction_week_len*2) + timedelta(minutes=1)
    val_lower_date = min_date + timedelta(weeks=train_week_len) 
 
    test_df = df_series.loc[(df_series.index > test_lower_date) & (df_series.index <= test_upper_date)]
    validation_df = df_series.loc[(df_series.index > val_lower_date) & (df_series.index <= test_lower_date)]
    training_df = df_series.loc[(df_series.index > min_date) & (df_series.index <= val_lower_date)]

    return training_df, validation_df, test_df    
    
    
## ETS model 
from statsmodels.tsa.holtwinters import ExponentialSmoothing

def find_best_ETS_model(train_data, test_data, value_col='GDP', seasonal_periods=[2, 4],\
            best_params=None):
    trend = ['add', 'mul']
    seasonal = ['add', 'mul']
    seasonal_periods = seasonal_periods
    if best_params:
        trend = [best_params[0]]
        seasonal = [best_params[1]]
        seasonal_periods = [best_params[2]]

    tr_se_seprd = list(itertools.product(trend, seasonal, seasonal_periods))
    best_ETS_MAE_model = None
    best_ETS_MAE_params = None
    best_MAE = float('inf')
    for tr, se, seprd in tr_se_seprd:
        try: 
            hwmodel = ExponentialSmoothing(train_data[value_col], trend=tr, seasonal=se, seasonal_periods=seprd).fit()
            test_pred = hwmodel.forecast(len(test_data))
            curr_MAE = mean_absolute_error(test_data[value_col], test_pred)
            if curr_MAE < best_MAE:
                best_ETS_MAE_params = (tr, se, seprd)
                best_MAE = curr_MAE
                best_ETS_MAE_model = hwmodel
        except:
            pass
    return best_ETS_MAE_model, best_MAE, best_ETS_MAE_params


## ARIMA model  
from statsmodels.tsa.arima.model import ARIMA

def find_best_ARIMA_model(train_data, test_data, value_col, p=range(0,9), q=range(0,9),\
     d=range(1,3), best_params=None):
    if best_params:
        p = [best_params[0]]
        d = [best_params[1]]
        q = [best_params[2]]

    pdq_combination = list(itertools.product(p,d,q))
    best_MAE = float('inf')
    best_ARIMA_MAE_model = None 
    best_pdq_MAE = None
    for pdq in pdq_combination:
        model_ARIMA = ARIMA(train_data[value_col], order=pdq)
        try:
            model_ARIMA_fit = model_ARIMA.fit()
            test_pred=model_ARIMA_fit.predict(start=test_data.index[0], end=test_data.index[-1])
            curr_MAE = mean_absolute_error(test_data[value_col], test_pred)
            if curr_MAE < best_MAE:
                best_pdq_MAE = pdq
                best_MAE = curr_MAE
                best_ARIMA_MAE_model = model_ARIMA_fit
        except:
            pass
    return best_ARIMA_MAE_model, best_MAE, best_pdq_MAE


## SARIMAX
from statsmodels.tsa.statespace.sarimax import SARIMAX

def find_best_SARIMAX_model(train_data, test_data, best_pdq, value_col='GDP',\
     season_period=4, best_params=None):
    P = range(0,3)
    Q = range(0,3)
    D = range(0,2)
    if best_params:
        P = [best_params[0]]
        D = [best_params[1]]
        Q = [best_params[2]] 
        season_period = best_params[3]

    PDQ_combination = list(itertools.product(P,D,Q))
    best_MAE = float('inf')
    best_SARIMAX_model = None 
    best_PDQS = None
    for P,D,Q in PDQ_combination:
        try:
            model_SARIMAX = SARIMAX(train_data[value_col], order=best_pdq, seasonal_order=(P,D,Q,season_period))
            model_SARIMAX_fit = model_SARIMAX.fit()
            test_pred = model_SARIMAX_fit.predict(start=test_data.index[0], end=test_data.index[-1])
            curr_MAE = root_mean_square_error(test_data[value_col], test_pred)
            if curr_MAE < best_MAE:
                best_PDQS = (P, D, Q, season_period)
                best_MAE = curr_MAE
                best_SARIMAX_model = model_SARIMAX_fit 
        except Exception as e:
            print(f"Exception: {e}")
    return best_SARIMAX_model, best_MAE, best_PDQS  


## Prophet 
# TODO: remove commented subsets of hyperparameters 
from prophet import Prophet

def create_proph_train_test(train_df, test_df, date_col='Date', value_col='GDP'):
    proph_train = train_df.rename(columns={date_col:'ds', value_col: 'y'})
    proph_test = test_df.rename(columns={date_col:'ds', value_col: 'y'})
    return proph_train, proph_test


# call with: proph_train, proph_test, test_data, value_col='GDP', freq='QS'
def find_best_Prophet_model(proph_train, proph_test, test_data, value_col, freq,\
        changepoint_range=[0.8, 0.9], changepoint_prior_scale=[0.05, 0.1, 0.3, 0.5],\
        seasonality_prio_scale=[0.1, 2, 10],\
        seasonality_mode=['additive', 'multiplicative'], best_params=None):
    if best_params:
        changepoint_range= [best_params[0]]
        changepoint_prior_scale= [best_params[1]]
        seasonality_prio_scale=[best_params[2]]
        seasonality_mode=[best_params[3]]
    changepoint_range_prior_scale_season_mode_comb = list(itertools.product(changepoint_range,\
         changepoint_prior_scale, seasonality_prio_scale, seasonality_mode))

    best_MAE = float('inf')
    best_Prophet_MAE_model = None 
    best_Prophet_MAE_params = None
    for changepoint_range, changepoint_prior_scale, seasonality_prio_scale, seasonality_mode in\
             changepoint_range_prior_scale_season_mode_comb:
        try:
            model_Prophet = Prophet(changepoint_range=changepoint_range,\
                                changepoint_prior_scale=changepoint_prior_scale,\
                                seasonality_prior_scale=seasonality_prio_scale,\
                                seasonality_mode=seasonality_mode)
            model_Prophet.fit(proph_train)
            future = model_Prophet.make_future_dataframe(periods=len(proph_test), freq=freq)
            forecast = model_Prophet.predict(future)
            test_pred = forecast[-(len(proph_test)):][['ds', 'yhat']].set_index('ds')
            curr_MAE = root_mean_square_error(test_data[value_col], test_pred)
            if curr_MAE < best_MAE:
                best_Prophet_MAE_params = (changepoint_range, changepoint_prior_scale,\
                     seasonality_prio_scale, seasonality_mode)
                best_MAE = curr_MAE
                best_Prophet_MAE_model = model_Prophet 
        except Exception as e:
            print(f"*** Exception occured: ", e)
            break; 

    return best_Prophet_MAE_model, best_MAE, best_Prophet_MAE_params


# save and load hyperparameters to local file 

def save_hyperparam(out_filename, the_tuple):
    """Save as comma separated integers or string"""
    pattern = r'\(|\)'
    str1 = re.sub(pattern, '', str(the_tuple))

    with open(out_filename, 'w') as f:
        f.write(f"{str1}\n")


def load_hyperparam(in_filename):
    """Read in the comma separated integers or strings converted to tuple"""
    with open(in_filename, 'r') as f:
        line = f.readline()
    def try_int(elem):
        try: 
            value = int(elem)
        except:
            try:
                value = float(elem)
            except:
                value = elem.replace("'", "").strip() 
        return value 

    return tuple(map(try_int, line.split(',')))