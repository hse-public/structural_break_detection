#!/usr/bin/env python

import itertools
import numpy as np
from sklearn import metrics

def root_mean_square_error(y_true, y_pred):
    return np.sqrt(metrics.mean_squared_error(y_true, y_pred))

def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

def timeseries_evaluation_metrics(y_true, y_pred):
    MSE = metrics.mean_squared_error(y_true, y_pred)
    MAE = metrics.mean_absolute_error(y_true, y_pred)
    RMSE = root_mean_square_error(y_true, y_pred)
    MAPE = mean_absolute_percentage_error(y_true, y_pred)
    R2_score = metrics.r2_score(y_true, y_pred)
    return MSE, MAE, RMSE, MAPE, R2_score   

# from https://github.com/inderjalli/wmape_grouping/blob/master/wmape_group_example.py
def wmape_metric(actual, forecast):
    # we take two series and calculate an output a wmape from it,
    # not to be used in a grouping function
    actual = actual.to_numpy()
    forecast = forecast.to_numpy()
    # make a series called mape
    se_mape = abs(actual-forecast)/actual
    # get a float of the sum of the actual
    ft_actual_sum = actual.sum()
    # get a series of the multiple of the actual & the mape
    se_actual_prod_mape = actual * se_mape
    # summate the prod of the actual and the mape
    ft_actual_prod_mape_sum = se_actual_prod_mape.sum()
    # float: wmape of forecast
    ft_wmape_forecast = ft_actual_prod_mape_sum / ft_actual_sum
    # return a float
    return ft_wmape_forecast


def timeseries_evaluation_metrics_func(y_true, y_pred):
    MSE, MAE, RMSE, MAPE, R2_score = timeseries_evaluation_metrics(y_true, y_pred) 
    wmape = wmape_metric(y_true, y_pred)

    print('Evaluation metric results:-')
    print(f'Mean Square Error is : {MSE}')
    print(f'Mean Absolute Error is : {MAE}')
    print(f'Root Mean Square Error is : {RMSE}')
    print(f'Mean Absolute Percentage Error is : {MAPE}')
    print(f'R2 score is : {R2_score}',end='\n\n') 
    print(f"Weighted MAPE: {wmape}")


## ETS model 
from statsmodels.tsa.holtwinters import ExponentialSmoothing

def find_best_ETS_model(train_data, test_data, val_col='GDP', seasonal_periods=[2, 4],\
            best_params=None):
    trend = ['add', 'mul']
    seasonal = ['add', 'mul']
    seasonal_periods = seasonal_periods
    if best_params:
        trend = [best_params[0]]
        seasonal = [best_params[1]]
        seasonal_periods = [best_params[2]]

    tr_se_seprd = list(itertools.product(trend, seasonal, seasonal_periods))
    best_ETS_RMSE_model = None
    best_ETS_RMSE_params = None
    best_ETS_WMAPE_model = None
    best_ETS_WMAPE_params = None
    best_RMSE = float('inf')
    best_WMAPE = float('inf')
    for tr, se, seprd in tr_se_seprd:
        try: 
            hwmodel = ExponentialSmoothing(train_data[val_col], trend=tr, seasonal=se, seasonal_periods=seprd).fit()
            test_pred = hwmodel.forecast(len(test_data))
            curr_RMSE = root_mean_square_error(test_data[val_col], test_pred)
            if curr_RMSE < best_RMSE:
                best_ETS_RMSE_params = (tr, se, seprd)
                best_RMSE = curr_RMSE
                best_ETS_RMSE_model = hwmodel
            curr_WMAPE = wmape_metric(test_data[val_col], test_pred)
            if curr_WMAPE < best_WMAPE:
                best_ETS_WMAPE_params = (tr, se, seprd)
                best_WMAPE = curr_WMAPE
                best_ETS_WMAPE_model = hwmodel
        except:
            pass
    return best_ETS_RMSE_model, best_RMSE, best_ETS_RMSE_params, best_ETS_WMAPE_model, best_WMAPE, best_ETS_WMAPE_params


## ARIMA model  
from statsmodels.tsa.arima.model import ARIMA

def find_best_ARIMA_model(train_data, test_data, val_col, p=range(0,16), q=range(0,16),\
     d=range(0,2), best_params=None):
    p = range(0,11)
    q = range(0,11)
    d = range(0,2)
    if best_params:
        p = [best_params[0]]
        d = [best_params[1]]
        q = [best_params[2]]

    pdq_combination = list(itertools.product(p,d,q))
    best_RMSE = float('inf')
    best_WMAPE = float('inf')
    best_ARIMA_RMSE_model = None 
    best_ARIMA_WMAPE_model = None 
    best_pdq_RMSE = None
    best_pdq_WMAPE = None 
    for pdq in pdq_combination:
        model_ARIMA = ARIMA(train_data[val_col], order=pdq)
        try:
            model_ARIMA_fit = model_ARIMA.fit()
            test_pred=model_ARIMA_fit.predict(start=test_data.index[0], end=test_data.index[-1])
            curr_RMSE = root_mean_square_error(test_data[val_col], test_pred)
            if curr_RMSE < best_RMSE:
                best_pdq_RMSE = pdq
                best_RMSE = curr_RMSE
                best_ARIMA_RMSE_model = model_ARIMA_fit
            curr_WMAPE = wmape_metric(test_data[val_col], test_pred)
            if curr_WMAPE < best_WMAPE:
                best_pdq_WMAPE = pdq 
                best_WMAPE = curr_WMAPE
                best_ARIMA_WMAPE_model = model_ARIMA_fit 
        except:
            pass
    return best_ARIMA_RMSE_model, best_RMSE, best_pdq_RMSE,\
        best_ARIMA_WMAPE_model, best_WMAPE, best_pdq_WMAPE


## SARIMAX
from statsmodels.tsa.statespace.sarimax import SARIMAX

def find_best_SARIMAX_model(train_data, test_data, best_pdq, val_col='GDP',\
     season_period=4, best_params=None):
    P = range(0,1)
    Q = range(0,1)
    D = range(0,1)
    if best_params:
        P = [best_params[0]]
        D = [best_params[1]]
        Q = [best_params[2]] 
        season_period = [best_params[3]]

    PDQ_combination = list(itertools.product(P,D,Q))
    best_RMSE = float('inf')
    best_SARIMAX_model = None 
    best_PDQS = None
    for P,D,Q in PDQ_combination:
        try:
            model_SARIMAX = SARIMAX(train_data[val_col], order=best_pdq, seasonal_order=(P,D,Q,season_period))
            model_SARIMAX_fit = model_SARIMAX.fit()
            test_pred = model_SARIMAX_fit.predict(start=test_data.index[0], end=test_data.index[-1])
            curr_RMSE = root_mean_square_error(test_data[val_col], test_pred)
            if curr_RMSE < best_RMSE:
                best_PDQS = (P, D, Q, season_period)
                best_RMSE = curr_RMSE
                best_SARIMAX_model = model_SARIMAX_fit 
        except:
            pass
    return best_SARIMAX_model, best_RMSE, best_PDQS  


## Prophet 
# TODO: remove commented subsets of hyperparameters 
from prophet import Prophet

def create_proph_train_test(train_data, test_data, date_col='Date', val_col='GDP'):
    train_df = train_data[[val_col]].reset_index()
    test_df = test_data[[val_col]].reset_index()
    proph_train = train_df.rename(columns={date_col:'ds', val_col: 'y'})
    proph_test = test_df.rename(columns={date_col:'ds', val_col: 'y'})
    return proph_train, proph_test


# call with: proph_train, proph_test, test_data, val_col='GDP', freq='QS'
def find_best_Prophet_model(proph_train, proph_test, test_data, val_col, freq,\
        changepoint_range=[0.8, 0.9], changepoint_prior_scale=[0.05, 0.1, 0.3],\
        seasonality_prio_scale=[2, 10],\
        seasonality_mode=['additive', 'multiplicative'], best_params=None):
    if best_params:
        changepoint_range= [best_params[0]]
        changepoint_prior_scale= [best_params[1]]
        seasonality_prio_scale=[best_params[2]]
        seasonality_mode=[best_params[3]]
    changepoint_range_prior_scale_season_mode_comb = list(itertools.product(changepoint_range,\
         changepoint_prior_scale, seasonality_prio_scale, seasonality_mode))

    best_RMSE = float('inf')
    best_WMAPE = float('inf')
    best_Prophet_RMSE_model = None 
    best_Prophet_RMSE_params = None
    best_Prophet_WMAPE_model = None 
    best_Prophet_WMAPE_params = None
    for changepoint_range, changepoint_prior_scale, seasonality_prio_scale, seasonality_mode in\
             changepoint_range_prior_scale_season_mode_comb:
        try:
            model_Prophet = Prophet(changepoint_range=changepoint_range,\
                                changepoint_prior_scale=changepoint_prior_scale,\
                                seasonality_prior_scale=seasonality_prio_scale,\
                                seasonality_mode=seasonality_mode)
            model_Prophet.fit(proph_train)
            future = model_Prophet.make_future_dataframe(periods=len(proph_test), freq=freq)
            forecast = model_Prophet.predict(future)
            test_pred = forecast[-(len(proph_test)):][['ds', 'yhat']].set_index('ds')
            curr_RMSE = root_mean_square_error(test_data[val_col], test_pred)
            if curr_RMSE < best_RMSE:
                best_Prophet_RMSE_params = (changepoint_range, changepoint_prior_scale,\
                     seasonality_prio_scale, seasonality_mode)
                best_RMSE = curr_RMSE
                best_Prophet_RMSE_model = model_Prophet 
            curr_WMAPE = wmape_metric(test_data[val_col], test_pred)
            print(f"current WMAPE values: ", curr_WMAPE)
            if curr_WMAPE < best_WMAPE:
                best_Prophet_WMAPE_params = (changepoint_range, changepoint_prior_scale,\
                    seasonality_prio_scale, seasonality_mode)
                best_WMAPE = curr_WMAPE
                best_Prophet_WMAPE_model = model_Prophet 
        except Exception as e:
            print(f"*** Exception occured: ", e)
            break; 

    return best_Prophet_RMSE_model, best_RMSE, best_Prophet_RMSE_params,\
        best_Prophet_WMAPE_model, best_WMAPE, best_Prophet_WMAPE_params