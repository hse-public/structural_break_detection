#!/usr/bin/env python

import itertools
import numpy as np
from sklearn import metrics

def timeseries_evaluation_metrics_func(y_true, y_pred):
    def mean_absolute_percentage_error(y_true, y_pred):
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        return np.mean(np.abs((y_true - y_pred) / y_true)) * 100
    print('Evaluation metric results:-')
    print(f'Mean Square Error is : {metrics.mean_squared_error(y_true, y_pred)}')
    print(f'Mean Absolute Error is : {metrics.mean_absolute_error(y_true, y_pred)}')
    print(f'Root Mean Square Error is : {np.sqrt(metrics.mean_squared_error(y_true, y_pred))}')
    print(f'Mean Absolute Percentage Error is : {mean_absolute_percentage_error(y_true, y_pred)}')
    print(f'R2 score is : {metrics.r2_score(y_true, y_pred)}',end='\n\n') 

def timeseries_RMSE_calc(y_true, y_pred):
    return np.sqrt(metrics.mean_squared_error(y_true, y_pred))


## ETS model 
from statsmodels.tsa.holtwinters import ExponentialSmoothing

def find_best_ETS_model(train_data, test_data, val_col='GDP', seasonal_periods=[2, 4]):
    trend = ['add', 'mul']
    seasonal = ['add', 'mul']
    seasonal_periods = seasonal_periods
    tr_se_seprd = list(itertools.product(trend, seasonal, seasonal_periods))
    best_ETS_model = None
    best_ETS_params = None
    best_RMSE = float('inf')
    for tr, se, seprd in tr_se_seprd:
        try: 
            hwmodel = ExponentialSmoothing(train_data[val_col], trend=tr, seasonal=se, seasonal_periods=seprd).fit()
            test_pred = hwmodel.forecast(len(test_data))
            curr_RMSE = timeseries_RMSE_calc(test_data[val_col], test_pred)
            if curr_RMSE < best_RMSE:
                best_ETS_params = (tr, se, seprd)
                best_RMSE = curr_RMSE
                best_ETS_model = hwmodel 
        except:
            pass
    return best_ETS_model, best_RMSE, best_ETS_params


## ARIMA model  
from statsmodels.tsa.arima.model import ARIMA

def find_best_ARIMA_model(train_data, test_data, val_col, p=range(0,16), q=range(0,16), d=range(0,2)):
    p = range(0,16)
    q = range(0,16)
    d = range(0,2)
    pdq_combination = list(itertools.product(p,d,q))
    best_RMSE = float('inf')
    best_ARIMA_model = None 
    best_pdq = None
    for pdq in pdq_combination:
        model_ARIMA = ARIMA(train_data[val_col], order=pdq)
        try:
            model_ARIMA_fit = model_ARIMA.fit()
            test_pred=model_ARIMA_fit.predict(start=test_data.index[0], end=test_data.index[-1])
            curr_RMSE = timeseries_RMSE_calc(test_data[val_col], test_pred)
            if curr_RMSE < best_RMSE:
                best_pdq = pdq
                best_RMSE = curr_RMSE
                best_ARIMA_model = model_ARIMA_fit 
        except:
            pass
    return best_ARIMA_model, best_RMSE, best_pdq


## SARIMAX
from statsmodels.tsa.statespace.sarimax import SARIMAX

def find_best_SARIMAX_model(train_data, test_data, best_pdq, val_col='GDP', season_period=4):
    P = range(0,1)
    Q = range(0,1)
    D = range(0,1)
    PDQ_combination = list(itertools.product(P,D,Q))
    best_RMSE = float('inf')
    best_SARIMAX_model = None 
    best_PDQS = None
    for P,D,Q in PDQ_combination:
        try:
            model_SARIMAX = SARIMAX(train_data[val_col], order=best_pdq, seasonal_order=(P,D,Q,season_period))
            model_SARIMAX_fit = model_SARIMAX.fit()
            test_pred = model_SARIMAX_fit.predict(start=test_data.index[0], end=test_data.index[-1])
            curr_RMSE = timeseries_RMSE_calc(test_data[val_col], test_pred)
            if curr_RMSE < best_RMSE:
                best_PDQS = (P, D, Q, season_period)
                best_RMSE = curr_RMSE
                best_SARIMAX_model = model_SARIMAX_fit 
        except:
            pass
    return best_SARIMAX_model, best_RMSE, best_PDQS  


## Prophet 
# TODO: remove commented subsets of hyperparameters 
from prophet import Prophet

def create_proph_train_test(train_data, test_data, date_col='Date', val_col='GDP'):
    train_df = train_data[[val_col]].reset_index()
    test_df = test_data[[val_col]].reset_index()
    proph_train = train_df.rename(columns={date_col:'ds', val_col: 'y'})
    proph_test = test_df.rename(columns={date_col:'ds', val_col: 'y'})
    return proph_train, proph_test


# call with: proph_train, proph_test, test_data, val_col='GDP', freq='QS'
def find_best_Prophet_model(proph_train, proph_test, test_data, val_col, freq, n_changepoints=[10, 25, 40, 50], yearly_seasonality=[5,10]):
    growth = ['linear',
              'logistic', 'flat']
    n_changepoints = n_changepoints
    seasonality_mode = ['additive', 
                        'multiplicative']
    yearly_seasonality = yearly_seasonality
    grow_n_change_sea_mode_year_sea_combination = list(itertools.product(growth, n_changepoints, seasonality_mode, yearly_seasonality))

    best_RMSE = float('inf')
    best_Prophet_model = None 
    best_Prophet_params = None
    for growth, n_changepoints, seasonality_mode, yearly_seasonality in grow_n_change_sea_mode_year_sea_combination:
        try:
            model_Prophet = Prophet(growth=growth, n_changepoints=n_changepoints, 
                                    seasonality_mode=seasonality_mode, yearly_seasonality=yearly_seasonality)
            model_Prophet.fit(proph_train)
            future = model_Prophet.make_future_dataframe(periods=len(proph_test), freq=freq)
            forecast = model_Prophet.predict(future)
            test_pred = forecast[-(len(proph_test)):][['ds', 'yhat']].set_index('ds')
            curr_RMSE = timeseries_RMSE_calc(test_data[val_col], test_pred)
            if curr_RMSE < best_RMSE:
                best_Prophet_params = (growth, n_changepoints, seasonality_mode, yearly_seasonality)
                best_RMSE = curr_RMSE
                best_Prophet_model = model_Prophet 
        except:
            pass

    return best_Prophet_model, best_RMSE, best_Prophet_params