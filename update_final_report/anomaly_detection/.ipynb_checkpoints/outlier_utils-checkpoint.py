#!/usr/bin/env python

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
from datetime import datetime, timedelta

# Keep the outliers
def keepOnlyOutliers(data, col):
    """Keep only outliers by using Q3 + 1.5 IQR and Q1 - 1.5 IQR"""
    Q3 = np.quantile(data[col], 0.75)
    Q1 = np.quantile(data[col], 0.25)
    IQR = Q3 - Q1
 
    #print("IQR value for column %s is: %s" % (col, IQR))
 
    lower_range = Q1 - 1.5 * IQR
    upper_range = Q3 + 1.5 * IQR
    #print(f"lower_range: {lower_range}")
    #print(f"upper_range: {upper_range}")
    outlier_list = [x for x in data[col] if (
        (x < lower_range) | (x > upper_range))]
    return data.loc[data[col].isin(outlier_list)] 


def createDataFrame(list_of_tuples, column_name):
    """create dataframes from list of mapes, dates tuples, pick end date"""
    df = pd.DataFrame(list(map(lambda x: (x[0], x[1][1]), list_of_tuples)),\
                      columns=[column_name, 'Date'])
    df.set_index('Date', inplace=True)
    return df 


def outlier_detection(option_df, anomalies):
    """
    Add column 'anomaly' to dataframe to mark outliers as True, non-outliers as False. 
    """
    # identify outliers
    option_df = option_df.assign(anomaly = False)
    for idx in anomalies:
        option_df.loc[idx, 'anomaly'] = True
    
    return option_df


def is_extreme_point(anomaly, df):
    """Check if this point is at the peak or valley of concave / convex"""
    two_days_ago = anomaly - timedelta(days=2)
    two_days_later = anomaly + timedelta(days=2)
    concave = df[anomaly:anomaly].PutCall_Ratio_Total.values[0] < df[two_days_ago:two_days_ago].PutCall_Ratio_Total.values[0]\
                and df[anomaly:anomaly].PutCall_Ratio_Total.values[0] < df[two_days_later:two_days_later].PutCall_Ratio_Total.values[0]
    convex = df[anomaly:anomaly].PutCall_Ratio_Total.values[0] > df[two_days_ago:two_days_ago].PutCall_Ratio_Total.values[0]\
                and df[anomaly:anomaly].PutCall_Ratio_Total.values[0] > df[two_days_later:two_days_later].PutCall_Ratio_Total.values[0]
    return concave or convex 


def calc_Precision(anomalies, df1):
    """Calculate Precision of anomaly extreme points"""
    TP_cnt = 0
    FP_cnt = 0
    for ano in anomalies:
        dateidx = pd.to_datetime(ano)
        if is_extreme_point(dateidx, df1):
            TP_cnt += 1
        else:
            FP_cnt += 1
    return float(TP_cnt/(TP_cnt + FP_cnt))


def find_best_outliers_window(df_mapes, outlier_windows, df, df_mapes_column=None):
    """Find best outliers window based on precision
       Returns: outlier_windows, precision, outliers 
    """
    if not df_mapes_column:
        df_mapes_column = df_mapes.columns[0]
    
    best_outliers_score = 0
    best_outliers_window = 0
    best_outliers = None
    
    for n in outlier_windows:
        upper_limit = df_mapes.shape[0] - n 
        anomalies = []
        for i in range(upper_limit):
            dfw = df_mapes[i:i+n]
            outlier = keepOnlyOutliers(dfw, df_mapes_column)
            if dfw[-1:].index.to_pydatetime()[0] in outlier.index.to_pydatetime():
                anomaly = outlier[-1:].index.to_pydatetime()[0] 
                anomalies.append(anomaly)
        score = calc_Precision(anomalies, df)
        if score > best_outliers_score:
            best_outliers_window = n
            best_outliers = anomalies
            best_outliers_score = score
        
    return best_outliers_window, best_outliers_score, best_outliers


def visualize_outliers(anomaly_detection, model_name, lower_date, upper_date):
    anomaly_detection = anomaly_detection[lower_date:upper_date]
    anomaly_detection.reset_index(inplace=True)
    # visualization of outliers detected
    fig, ax = plt.subplots(figsize=(15,5))
    a = anomaly_detection[anomaly_detection['anomaly'] == True]  # anomaly
    ax.plot(anomaly_detection.Date, anomaly_detection['PutCall_Ratio_Total'], color='black', label = 'Normal')
    ax.scatter(a.Date, a['PutCall_Ratio_Total'], color='red', label = 'Anomaly')
    plt.title(f'Structural Break Detection with {model_name}')
    plt.legend()
    plt.show()