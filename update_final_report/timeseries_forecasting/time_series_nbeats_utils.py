#!/usr/bin/env python

import pandas as pd
import torch
from datetime import datetime, timedelta
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping

from pytorch_lightning.loggers import TensorBoardLogger

from pytorch_forecasting import NBeats, TimeSeriesDataSet
from pytorch_forecasting.metrics import MAE, MAPE, MASE, SMAPE

from time_series_utils import get_nth_train_split

def preprocess_nbeats(df1):
    df1['group'] = 1
    df1.reset_index(inplace=True)
    df1['time_idx'] = (df1.Date - df1.Date.min()).dt.days 
    df1.set_index(['Date'], inplace=True)
    df1.index = pd.DatetimeIndex(df1.index.values, freq='D', name='Date')
    return df1 


def compute_mape(training_df, prediction_test_df, y, y_hat):
    """
    Compute the MAPE for that time series and return the last 7 training PutCall_Ratio_Total
    """
    training_df_series = training_df
    training_df_series.sort_values(by='Date', axis=0, ascending=False, inplace=True)
    prediction_test_df_series = prediction_test_df
    prediction_test_df_series['abs_pct_error'] = ((prediction_test_df_series[y] - prediction_test_df_series[y_hat])/prediction_test_df_series[y]).abs()
    mape = prediction_test_df_series['abs_pct_error'].mean()
    return mape


def nbeats_modeler(training_df, validation_df, test_df, 
                 max_prediction_length=7, max_encoder_length=27*7,
                 num_blocks=[1,1], num_block_layers=[3,3],
                 expansion_coefficient_lengths=[3,3], 
                 batch_size=256, max_epochs=50, loss=MAPE()):    
    """
    Return the N-BEATS model, trainer and dataloaders given the training, validation 
    and test dataframes, and parameters 
    """

    context_length = max_encoder_length
    prediction_length = max_prediction_length

    # calculate the time indexes that the validation and test data start from
    val_idx = validation_df['time_idx'].min()
    test_idx = test_df['time_idx'].min()

    # setup Pytorch Forecasting TimeSeriesDataSet for training data
    training_data = TimeSeriesDataSet(
        training_df,
        time_idx="time_idx",
        target="PutCall_Ratio_Total",
        group_ids=["group"],
        time_varying_unknown_reals=["PutCall_Ratio_Total"],
        max_encoder_length=context_length,
        max_prediction_length=prediction_length
    )

    # setup Pytorch Forecasting TimeSeriesDataSet for validation and test data
    validation_data = TimeSeriesDataSet.from_dataset(training_data, pd.concat([training_df, validation_df]), min_prediction_idx=val_idx)
    test_data = TimeSeriesDataSet.from_dataset(training_data, pd.concat([training_df, validation_df, test_df]), min_prediction_idx=test_idx)

    # convert data to dataloader
    train_dataloader = training_data.to_dataloader(train=True, batch_size=batch_size)
    val_dataloader = validation_data.to_dataloader(train=False, batch_size=batch_size)
    test_dataloader = test_data.to_dataloader(train=False, batch_size=batch_size)

    pl.seed_everything(42)  # set seed

    early_stop_callback = EarlyStopping(monitor="val_loss", min_delta=1e-4, patience=10, verbose=False, mode="min")

    # checkpoint_callback = ModelCheckpoint(monitor="val_loss")  # Init ModelCheckpoint callback, monitoring 'val_loss'
    logger = TensorBoardLogger("training_logs")  # log to tensorboard

    # setup PyTorch Lightning Trainer
    trainer = pl.Trainer(
        max_epochs=max_epochs,
        gpus=torch.cuda.device_count(),
        gradient_clip_val=1,
        callbacks=[early_stop_callback],
        logger=logger,
    )

    # setup NBEATS model using PyTorch Forecasting's N-Beats class
    model = NBeats.from_dataset(
        training_data,
        num_blocks=num_blocks,
        num_block_layers=num_block_layers,
        expansion_coefficient_lengths=expansion_coefficient_lengths,
        learning_rate=0.0001,
        log_interval=-1,
        log_val_interval=1,
        weight_decay=1e-2,
        #widths=[256, 1024],
        widths=[32, 512],
        loss=loss,
        logging_metrics=torch.nn.ModuleList([MAPE()]),
    )

    return model, trainer, train_dataloader, val_dataloader, test_dataloader


def return_nbeats_predictions(trainer, test_dataloader, test_df):
    """
    Given the trained trainer, test_dataloader and dataframe, 
    return a dataframe containing the actual and corresponding predicted values
    """
    # extract predictions from best model
    best_model_path = trainer.checkpoint_callback.best_model_path
    best_model = NBeats.load_from_checkpoint(best_model_path)
    predictions, index = best_model.predict(test_dataloader, return_index=True)

    # merge predictions and actual data into single dataframe
    time_idx_start = index.loc[0, 'time_idx']
    time_idx_end = time_idx_start + len(predictions[0])
    predictions_df_wide = pd.DataFrame(predictions.numpy(), columns=range(time_idx_start, time_idx_end))
    predictions_df_wide['group'] = index['group']
    predictions_df = predictions_df_wide.melt(id_vars=['group'])
    predictions_df.rename(columns={'variable':'time_idx'}, inplace=True)
    nbeats_test_df = test_df.merge(predictions_df, on=['group','time_idx'])
    nbeats_test_df.rename(columns={'PutCall_Ratio_Total': 'nbeats_pred'}, inplace=True)
    return nbeats_test_df



def train_n_weeks_for_3years(df1, train_weeks=14):
    mape_list = []
    steps = 156  # number of windowing steps (unit week)

    pred_df = None
    
    for i in range(steps):
        training_df, validation_df, test_df = get_nth_train_split(df1, i, train_week_len=train_weeks)

        model, trainer, train_dataloader, val_dataloader, test_dataloader = nbeats_modeler(training_df, validation_df,\
                                                                                           test_df, max_encoder_length=(train_weeks-1)*7)

        # fine tune learning rate hyperparameter
        res = trainer.tuner.lr_find(model, train_dataloaders=train_dataloader, val_dataloaders=val_dataloader,\
                                    min_lr=1e-5)
        print(f"suggested learning rate: {res.suggestion()}")
        model.hparams.learning_rate = res.suggestion()

        trainer.fit(
            model,
            train_dataloaders=train_dataloader,
            val_dataloaders=val_dataloader,
        )

        nbeats_test_df = return_nbeats_predictions(trainer, test_dataloader, test_df)

        if i == 0:
            pred_df = nbeats_test_df['nbeats_pred']
        else:
            pred_df = pd.concat([pred_df, nbeats_test_df['nbeats_pred']])
        mape = compute_mape(training_df, nbeats_test_df, 'value', 'nbeats_pred')
        mape_list.append((mape, (test_df.index[0], test_df.index[-1])))

    return mape_list, pred_df          