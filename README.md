# Structural Break Detection

Structural Break Detection

Update Final Report (6 June 2022)

* Main folder: update_final_report 
* Start with preprocessing folder to read in CSV files 
* Continue with timeseries_forecaseting folder to training and test timeseries forecasting (some ran for many hours depending on platform, GPU is required for nbeats model)
* Structural Break detection is within folder anomaly_detection


Update 5: (27 May 2022)
(1) We work with Options datasets.
(2) We train, validate, test against 14 weeks moving windows for 156 weeks.
(3) Before we train we tune hyperparameters for 4 models (Arima, ETS, Prophet and N-Beats Deep Learning models)
(4) We find the outlier MAPEs values if exist on last week of 12 weeks window and plot against the Options datasets. 
(5) Details in the documentation. (PDF in root level of this repo)


Update 4: (12 May 2022) 
(1) We use WMAPE (weight mean absolute percentage error) as performance matrix so we could compare the quality of different timeseries estimation. 
(2) We update/fix hyperparameter tuning for SARIMAX and Prophet. 
(3) We split the dataset into training, validation and test data. Tune hyperparameter with training and validation. Then train with training+validation and use it to estimate the span of test data. The quality matrix was measured against the test data. 
(4) We are going to write up the documentation of work so far before the meeting with advisor. 


Update 3:
Progress (April 25, 2022):
(1) We limit time series models to ARIMA, SARIMAX, Holt Winters Exponential Smoothings (ETS), and Prophet
(2) On all 6 dataset above in update 2, we tune the hyperparameters for each 3 models we keep. 
(3) We create script to automate the hyperparameters tuning to find the best model based on the lowest RMSE. 
Note:  Missing in update 3:
  We supposed to also perform one step ahead forecast, but we run out of time. So we will do that on next sprint. 


Update_2: 
Progress (April 9, 2022):
(1) Create time series models using ARIMA, SARIMAX, Prophet, Orbit DLT (Damp Local Trend), and DeepAR
(2) We train and predict each models above on times series data of:
    - Stock Index 
    - Google Earnings
    - Forex CNY RUB
    - Manufacturing Order Durable Goods
    - China Real GDP 
    - Non-Farm Payroll


Update_1: 
Progress (March 26, 2022): 
(1) Created 2 time series models using Prophet and DeepAR on Global Plam Oil prices 




 
## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/hse-public/structural_break_detection.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/hse-public/structural_break_detection/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
